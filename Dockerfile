# This is a multistage build.

# Helper image for compiling durrs
FROM registry.gitlab.com/yakshaving.art/dockerfiles/go-builder:master as installer

ENV HURRDURR_TAG=0.1.6

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

# hadolint ignore=DL3018,DL3003
RUN apk -qq --no-cache add binutils \
	# compile hurrdurr
	&& git clone https://gitlab.com/yakshaving.art/hurrdurr.git \
	&& ( cd /hurrdurr \
		&& git checkout ${HURRDURR_TAG} \
		&& CGO_ENABLED=0 go build \
		&& strip hurrdurr ) \
	&& mv /hurrdurr/hurrdurr /usr/local/bin \
	# install other things
	&& echo "done"

# This is our final image, based on base image
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master
LABEL version="0.0.1" \
      maintaner="Ilya A. Frolov <if+gitlab@solas.is>" \
      description="Image we use when we need som durrin"

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

COPY --from=installer --chown=root:root \
	/usr/local/bin/hurrdurr \
	/usr/local/bin/

# hadolint ignore=DL3013,DL3018
RUN apk -qq --no-cache add py3-pip python3 \
	&& pip3 --no-cache-dir install yamllint \
	# fix permissions
	&& ( set +f; chown root:root /usr/local/bin/* \
		&& chmod 0755 /usr/local/bin/* ) \
	# install other things below
	&& apk -qq --no-cache del py3-pip \
	&& echo "done"
